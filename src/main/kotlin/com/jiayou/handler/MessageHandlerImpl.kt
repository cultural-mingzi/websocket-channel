package com.jiayou.handler

import cn.hutool.core.util.StrUtil
import cn.hutool.json.JSONUtil
import com.jiayou.WebSocketEndpoint.Companion.configurableApplicationContext
import com.jiayou.autoconfig.WebSocketAutoConfig
import com.jiayou.channel.OnRecive
import com.jiayou.channel.OnSend
import com.jiayou.enmu.StatusCode


class MessageHandlerImpl<Any> : MessageHandler<Any> {

    override fun doWork(onReceive: OnRecive): OnSend {
        var data: Any? = null        //返回的数据
        var type: String = ""        //返回类型
        var exception: String = ""   //异常情况
        var a = 0
        //                               父端点名         包名+类名             子端点名   方法名
        //        val endpointMap = HashMap<String, HashMap<String, List<HashMap<String, String>>>>()
        try {
            WebSocketAutoConfig.endpointMap.forEach { t, u ->
                if (onReceive.intent.split("-")[0] == t) {
                    u.forEach { (_t, _u) ->
                        _u.forEach {
                            it.forEach { (t, u) ->
                                if (onReceive.intent.split("-")[1] == t) {
//                                    _t -> class
                                    _t.methods.forEach { method ->
                                        if (method.name == u) {
                                            type = method.annotatedReturnType.type.typeName
                                            if (method.parameterCount == 0)
                                                if (type != "void")
                                                    data = method.invoke(configurableApplicationContext?.getBean(_t)) as Any?
                                                else
                                                    method.invoke(configurableApplicationContext?.getBean(_t))
                                            else
                                                if (type != "void") {
                                                    data = method.invoke(configurableApplicationContext?.getBean(_t),
                                                            JSONUtil.toBean(onReceive.data, method.parameterTypes[0]))
                                                            as Any?
                                                } else {
                                                    method.invoke(configurableApplicationContext?.getBean(_t),
                                                            JSONUtil.toBean(onReceive.data, method.parameterTypes[0]))
                                                }
                                            a++
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            exception = e.message.toString()
            e.printStackTrace()
        }

        if (a == 0)
            return OnSend(onReceive.id, onReceive.from, StatusCode.NoHandler, null)
        return if (StrUtil.isBlank(exception))
            if (StrUtil.isBlank(type))
                OnSend(onReceive.id, onReceive.from, StatusCode.Success, null)
            else
                OnSend(onReceive.id, onReceive.from, StatusCode.Success, data)
        else
            OnSend(onReceive.id, onReceive.from, StatusCode.Fail, null)
    }
}
