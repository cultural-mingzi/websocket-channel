package com.jiayou.channel

import com.jiayou.enmu.DataType
import com.jiayou.enmu.StatusCode

/**
 * @param id             -> UUID
 * @param from           -> 发送者
 * @param intent         -> 意图
 * @param parameterType  -> 表单数据类型
 * @param data           -> 接收参数
 */
data class OnRecive(var id: String, var from: String, var intent: String, var parameterType: DataType, var data: String)


/**
 * @param id    -> UUID
 * @param to    -> 给谁返回
 * @param code  -> 状态码
 * @param data  -> 返回的数据
 */
data class OnSend(var id: String, var to: String, var code: StatusCode, var data: Any?)


data class ParameterData<T>(var t: T?)
data class ReturnData<T>(var code: Int, var message: String, var t: T?)