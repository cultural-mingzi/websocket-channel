package com.jiayou.enmu

/**
 * 从前台接收数据的数据类型
 */
enum class DataType {
    Text, Json, Byte, Any
}