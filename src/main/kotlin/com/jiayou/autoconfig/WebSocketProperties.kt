package com.jiayou.autoconfig

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component


@ConfigurationProperties("websocket-channel")
data class WebSocketProperties(var log: Boolean?)